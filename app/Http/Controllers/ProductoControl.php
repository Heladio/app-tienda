<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
class ProductoControl extends Controller
{
    //
    public function index()
    {
    	$product = Producto::paginate(10);
    	return view('admin/productos/index')->with(compact('product')); // Listado
    }
    public function crear()
    {
    	return view('admin/productos/crear'); //formulario de registro
    }
    public function store(Request $request)
    {
        $mens=[
            'nom.required' => "Es necesario un Nombre del Producto",
            'nom.min' => "El Nombre debe tener al menos 3 caracteres",

            'desc.required' => "La descripción corta del Producto es obligatoria",
            'desc.max' => "La descripción no puede tener mas de 200 caracteres",
            
            'prec.required' => "El Precio del Producto es obligatorio",
            'prec.numeric' => "Solo se aceptan numeros en el campo de Precio",
            'prec.min' => "El Precio debe tener al menos 1 numero",
        ];
        $rules=[
            'nom'=> 'required|min:3',
            'desc'=> 'required|max:200',
            'prec'=> 'required|numeric|min:0',
        ];
        $this->validate($request, $rules,$mens);
    	//registrar nuevo producto
    	//dd($request->All());
    	$p= new Producto();
    	$p->nombre=$request->input('nom');
    	$p->descripcion=$request->input('desc');
    	$p->descripcion_larga=$request->input('descx');
    	$p->precio=$request->input('prec');
    	$p->save();
    	return redirect('/admin/productos');
    }
    public function editar($id)
    {   
        //return "Mostrar el form de edit con id $id";
        $mens=[
            'nom.required' => "Es necesario un Nombre del Producto",
            'nom.min' => "El Nombre debe tener al menos 3 caracteres",

            'desc.required' => "La descripción corta del Producto es obligatoria",
            'desc.max' => "La descripción no puede tener mas de 200 caracteres",
            
            'prec.required' => "El Precio del Producto es obligatorio",
            'prec.numeric' => "Solo se aceptan numeros en el campo de Precio",
            'prec.min' => "El Precio no puede ser negativo",
        ];
        $rules=[
            'nom'=> 'required|min:3',
            'desc'=> 'required|max:200',
            'prec'=> 'required|numeric|min:0',
        ];
        $this->validate($request, $rules,$mens);
        $p = Producto::find($id);
        return view('admin.productos.editar')->with(compact('p')); //formulario de edicion
    }
    public function actualizar(Request $request,$id)
    {
        $p= Producto::find($id);
        $p->nombre=$request->input('nom');
        $p->descripcion=$request->input('desc');
        $p->descripcion_larga=$request->input('descx');
        $p->precio=$request->input('prec');
        $p->save();
        return redirect('/admin/productos');
    }
    public function destruir($id)
    {
        $p= Producto::find($id);
        $p->delete();
        return redirect('/admin/productos');
    }
}
