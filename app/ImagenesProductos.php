<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenesProductos extends Model
{
    //
    public function producto()
    {
    	return $this->belongsTo(Producto::class);
    }
}
