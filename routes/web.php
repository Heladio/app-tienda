<?php

Route::get('/', 'TestController@bienvenido');

Route::get('/prueba', function (){
    return 'Hola soy la ruta de prueba';
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth','admin'])->prefix('admin')->group(function (){
	Route::get('/productos', 'ProductoControl@index'); //listado
	Route::get('/productos/crear', 'ProductoControl@crear'); //crear
	Route::post('/productos', 'ProductoControl@store'); //crear
	Route::get('/productos/{id}/editar', 'ProductoControl@editar'); //editar
	Route::post('/productos/{id}/editar', 'ProductoControl@actualizar'); //actualizar
	Route::delete('/productos/{id}', 'ProductoControl@destruir'); //ELIMINAR
});


