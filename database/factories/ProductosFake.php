<?php

use Faker\Generator as Faker;
use App\Producto;
$factory->define(Producto::class, function (Faker $faker) {
    return [
        'nombre' => substr($faker-> sentence(3), 0, -1),
        'descripcion' => $faker->sentence(10),
        'descripcion_larga' =>$faker-> text,
        'precio' => $faker->randomfloat(2,5,150),
        'categoria_id' => $faker -> numberBetween(1,5)
    ];
});
