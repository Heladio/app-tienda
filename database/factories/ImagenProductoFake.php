<?php

use Faker\Generator as Faker;
use App\ImagenesProductos;
$factory->define(ImagenesProductos::class, function (Faker $faker) {
    return [
        'imagen' => $faker -> imageUrl(250,250),
        'producto_id' => $faker -> numberBetween(1,60)
    ];
});
