<?php

use Illuminate\Database\Seeder;
use App\Categoria;
use App\Producto;
use App\ImagenesProductos;
class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $categorias = factory (Categoria::class , 5) -> create();
      $categorias->each(function ($c){
        $product = factory(Producto::class, 20)->make();
        $c->producto()->saveMany($product);

        $product->each(function ($i)
        {
          $imagenes=factory(ImagenesProductos::class, 5)->make();
          $i->imagenes()->saveMany($imagenes);
        });
      });

      /*
       factory(Categoria::class, 5)->create();
       factory(Producto::class, 60)->create();
       factory(ImagenesProductos::class, 150)->create();
       */


    }
}


/*

$cate = factory(Categoria::class, 5) -> create();
       $cate->each(function ($c) {
        $productos=factory(Producto::class, 20)->make();
        $c->productos()->saveMany($productos);

        $productos->each(function ($p){
          $ima=factory(ImagenesProductos::class, 5)->make();
        $p-> ima()->saveMany($ima);
        });
       });
*/