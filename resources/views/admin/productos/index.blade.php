@extends('layouts.app')

@section('title','Listado de Producto')
@section('body-class' , 'product-page')
@section('content')
<div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
</div>

<div class="main main-raised">
<div class="container">
<div class="section text-center">
<h2 class="title">Listado de Productos</h2>
<div class="team">
<div class="row">

<a href="/admin/productos/crear" class="btn btn-primary btn-round">Nuevo Producto</a>

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Categoria</th>
            <th class="text-right">Precio</th>
            <th class="text-right">Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($product as $pro)
        <tr>
            <td class="text-center">{{ $pro->id }}</td>
            <td>{{ $pro->nombre }}</td>
            <td class="col-md-4">{{ $pro->descripcion }}</td>
            <td>{{ $pro->categoria ? $pro->categoria->nombre : 'General' }}</td>
            <td class="text-right">&#36; {{ $pro->precio }}</td>
            <td class="td-actions text-right">
                <button type="button" rel="tooltip" title="View Profile" class="btn btn-info btn-simple btn-xs">
                    <i class="fa fa-info"></i>
                </button>
                <a href="{{ url('/admin/productos/'.$pro->id.'/editar') }}" rel="tooltip" title="Edit Profile" class="btn btn-success btn-simple btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <form method="post" action="{{ url('/admin/productos/'.$pro->id.'') }}">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                    <i class="fa fa-times"></i>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $product->links() }}
</div></div></div></div></div>
@include('includes.footer')
@endsection
