@extends('layouts.app')

@section('title','Editar Producto')
@section('body-class' , 'product-page')
@section('content')
<div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
            
        </div>

        <div class="main main-raised">
            <div class="container">
                <div class="section">
                    <h2 class="title">Editar Producto Seleccionado</h2>

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->All() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{ url('/admin/productos/'.$p->id.'/editar') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre del Producto</label>
                                    <input type="text" class="form-control" name="nom" value="{{ old('nom'), $p->nombre }}">
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Precio del producto</label>
                                    <input type="number" step="0.01" class="form-control" name="prec" value="{{  old('prec'), $p->precio }}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Descripcion corta</label>
                                    <input type="text" class="form-control" name="desc" value="{{ old('desc'), $p->descripcion }}">
                                </div>
                            </div>
                        </div>                        
                                                                
<textarea class="form-control" placeholder="Descripcion Extensa del Producto" name="descx" rows="5"> {{ old('descx'), $p->descripcion_larga }} </textarea>
                        <button class="btn btn-primary" type="submit">Guardar Cambios</button>
                        <a class="btn btn-primary" href="{{ url('/admin/productos') }}" >Cancelar</a>
                    </form>

                </div>

                </div>
            </div>

        </div>

@include('includes.footer')
@endsection
